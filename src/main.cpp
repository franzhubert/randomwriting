
#include "allinc.hpp"
#include "defines.hpp"

char possible_chars[92][2] = {"!","\"","#","$","%","&","'","(",")","*","+",",","-",".","/","0","1","2","3","4","5","6","7","8","9",":",";","<","=",">","?","@","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","[","\\","]","^","_","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","r","s","t","u","v","w","x","y","z","{","|","}","~"};

int main(int argc, char** argv)
{
	if(argc < 2)
	{
		printf("not enough arguments given, syntax is:\n");
		printf("	%s <count>\n", argv[0]);
		return 1;
	}

	printf("Get ready:\n");
	for(int i=2; i>=0; i--)
	{
		usleep(999999);
		printf("--- %d ---\n", i);
	}

	srand(time(0));

	system("stty raw");

	int count = atoi(argv[1]);
	int typed_count = 0;
	int this_char_random = 0;
	char* this_char;
	char input;

	auto start_time = std::chrono::steady_clock::now();

	for(int index=0; index<count; index++)
	{
		this_char_random = rand()%92;
		this_char = &possible_chars[this_char_random][0];
		printf("%s\n\033[1G", this_char);
		while(input != *this_char)
		{
			input = getchar();
			typed_count++;
		}
		printf("\n\033[1G");
	}

	system("stty cooked");

	auto end_time = std::chrono::steady_clock::now();

	std::chrono::duration<float> elapsed_time = end_time - start_time;

	printf("stats:\n");
	printf("elapsed Time: %f seconds\n", elapsed_time.count());
	printf("written:      %d characters\n", count);
	printf("typed:        %d characters\n", typed_count);
	printf("accuracy:     %f%%\n", (float)count/(float)typed_count*100.0);
	printf("cpm / real:   %f / %f\n", (float)count/elapsed_time.count(), (float)typed_count/elapsed_time.count());

	return 0;
}
